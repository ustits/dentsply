<?php 
    header("Content-Type: text/html; charset=utf-8");
?>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style/style.css" />
        <link href="scripts/select2-3.4.3/select2.css" rel="stylesheet"/>
        <script src="scripts/jquery-1.10.js"></script>
        <script type="text/javascript" src="scripts/script.js"></script>
        <script src="scripts/select2-3.4.3/select2.js"></script>
        <script src="scripts/jquery.validate.min.js"></script>
        <script src="scripts/list.js"></script>
        <script src="scripts/listpag.js"></script>
        <script src="scripts/jeditable.js"></script>
        <title>Меню</title>
    </head>
    <body>
        <script>
            $.post('scripts/php/checkLogin.php', function(response) {
                var temp = response;
                var userInfo = JSON.parse(temp);
                if (userInfo.log != 'logged') {
                    window.location.replace("login.php");
                } else {
                    userID = userInfo.id;
                    userName = userInfo.name;
                    var userDiv = '<div class="user">Здравствуйте, ' + userName + '</div>'
                    $('#calendar').after(userDiv);
                }
            });

            loadForm();      
        </script>
        <div class="header">
            <div onClick="headerFunction();">Dentsply</div>
                <img src="images/blue_settings.png" alt="Настройки" height="30" width="30" align="right" id="setBut" />
                <img src="images/blue_calendar.png" alt="Задачи" heigh="30" width="30" align="right" id="calBut" />
        </div>
        <div id="settings">
            <ul>
                <li>Настройки</li>
                <li onClick="exit();">Выход</li>
            </ul>
        </div>
        <div id="calendar">
            <ul>
                <li onClick="createContent('tasks', 'taskList');">Список задач</li>
                <li onClick="createContent('tasks', 'newTask');">Новая задача</li>
            </ul>
        </div>
        
        <div class="nav-wrapper">
            <div class="nav">
                <div class="nav-head">Меню</div>
            </div>
            <div class="menu">
                <ul>
                    <div class="addSection">
                        <li>Добавление</li>
                        <div class="addSubSection">
                            <li onClick="createContent('add', 'newPlanner');">Новый визит</li>
                            <li onClick="createContent('add', 'newOrder');">Новый заказ</li>
                            <li onClick="createContent('add', 'newClinic');">Новая клиника</li>
                            <li onClick="createContent('add', 'newDoctor');">Новый доктор</li>
                            <li onClick="createContent('add', 'newNetwork');">Новая сеть</li>
                            <li onClick="createContent('add', 'newEducation');">Запись на курсы</li>
                            <li onClick="createContent('add', 'newCourse');">Новые курсы</li>
                            <li onClick="createContent('add', 'newRegion');">Новый регион</li>
                            <li onClick="createContent('add', 'newTown');">Новый город</li>
                        </div>
                    </div>
      
                    <div class="editSection">
                        <li>Редактирование</li>
                        <div class="editSubSection">
                            <li onClick="createContent('edit', 'planner');">Планировщик</li>
                            <li onClick="#">Пользователи</li>
                            <li onClick="#">Доктора</li>
                            <li onClick="#">Планировщик</li>
                            <li onClick="#">Планировщик</li>
                        </div>
                    </div>
    
                    <div class="reportSection">
                        <li>Отчеты</li>
                        <div class="reportSubSection">
                            <li onClick="#">Ежедневный отчет</li>
                            <li onClick="createContent('reports', 'showProgress');">По сотрудникам</li>
                            <li onClick="createContent('reports', 'showProducts');">Список продукции</li>
                            <li onClick="createContent('reports', 'showDistributors');">Дистрибьюторы</li>
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    
        <div class="content-wrapper">
            <div class="content">
                <div class="content-head">Добро Пожаловать</div>
            </div>
        <div class="workspace"></div>
        </div>
    
        <div class="footer"><a href="#">Copyright © 2013 Ustits Ruslan</a></div>
    </body>
</html>