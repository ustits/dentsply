function exit() {
	var x = confirm("Вы действительно хотите выйти?");
	if (x == true) {
		document.location.href = "login.php";
	}
}

/*          Database response             */
function processData(response) {
    if (response == '') {
        $(':input[name=postButton]').after('<p>Информация успешно добавлена!</p>');
    } else {
        $(':input[name=postButton]').after("<p class='error'>Ошибка! Не удалось добавить информацию!</p>");
    }
        
}

function loadForm() {
	$(document).ready(function() {
        $('.addSubSection').hide();
		$('.editSubSection').hide();
		$('.reportSubSection').hide();
        
        $('#settings').hide();
        $('#calendar').hide();
        
        $('#setBut').click(function() {
            $('.user').slideUp(150);
            $('#settings').delay(150).slideToggle(150);
            $('#calendar').hide();
            if ($('.user').css('display') == 'none') {
                $('.user').delay(300).slideDown(150);
            }
            
        });
        $('#calBut').click(function() {
            $('.user').slideUp(150);
            $('#calendar').delay(150).slideToggle(150);
            $('#settings').hide();
            if ($('.user').css('display') == 'none') {
                $('.user').delay(300).slideDown(150);
            }
            
        });
        $('#settings li').click(function() {
            $('#settings').slideUp(150);
            $('.user').delay(150).slideDown(150);
        });
        $('#calendar li').click(function() {
            $('#calendar').slideUp(150);
            $('.user').delay(150).slideDown(150);
        });
        
        $('.addSection').click(function() {
            $('.addSubSection').slideToggle(300);
        });
        $('.editSection').click(function() {
            $('.editSubSection').slideToggle(300);
        });
        $('.reportSection').click(function() {
            $('.reportSubSection').slideToggle(300);
        });
	});
}

function headerFunction() {
	$('.content-head').html('Добро Пожаловать');
    $('.workspace').html('');
}

function createContent(section, subsection) {
    $('.workspace').load('content/' + section + '/' + subsection + '.php');
}


