<?php 
    require 'scripts.php';
    Connection('localhost', 'root', '', 'dentsply'); 

    $clinicName = $_POST['clinicName'];
    $netID = $_POST['network'];
    $townID = $_POST['town'];
    $address = $_POST['address'];
    $category = $_POST['category'];
    $chairs = $_POST['chairs'];
    $docCount = $_POST['doctorCount'];
    $passability = $_POST['passability'];
    $restoration = $_POST['restoration'];
    $canalsCount = $_POST['canalsCount'];
    $purchase = $_POST['purchase'];
    $priceLevel = $_POST['priceLevel'];
    $orderFreq = $_POST['orderFreq'];
    $orderDays = $_POST['orderDays'];
    $tel1 = $_POST['tel1'];
    $telType1 = $_POST['telType1'];
    $tel2 = $_POST['tel2'];
    $telType2 = $_POST['telType2'];
    $tel3 = $_POST['tel3'];
    $telType3 = $_POST['telType3'];
    $district = $_POST['district'];
    $email = $_POST['email'];
    $www = $_POST['www'];
    $worksNow = $_POST['worksNow'];
    $notes = $_POST['notes'];
    
    //Adding clinic info to t_clinics
    mysql_query("INSERT INTO `dentsply`.`t_clinics` (`clinicName`, `netID`, `townID`, `address`, `district`, `category`, `chairs`, `docCount`, `passability`, `restoration`, `canalsCount`, `purchase`, `priceLevel`, `orderFreq`, `orderDays`, `www`, `worksNow`, `Notes`) VALUES('$clinicName', '$netID', '$townID', '$address', '$district', '$category', '$chairs', '$docCount', '$passability', '$restoration', '$canalsCount', '$purchase', '$priceLevel', '$orderFreq', '$orderDays', '$www', '$worksNow', '$notes')") or die ("Error: Can`t add clinic info! " . mysql_error());
    $clinicID = mysql_insert_id();
    //Adding telephone to t_telephone
    mysql_query("INSERT INTO `dentsply`.`t_telephone` (`teleTypeID`, `telephone`) VALUES ('$telType1', '$tel1')") or die ("Error: Can`t add telephone! " . mysql_error());
    $telID = mysql_insert_id();
    //Connecting telephone to clinic via t_telelist
    mysql_query("INSERT INTO `dentsply`.`t_telelist` (`teleID`, `clinicID`, `staffID`) VALUES ('$telID', '$clinicID', '0')") or die ("Error: Can`t connect telephone to clinic! " . mysql_error());
    if ($docTel2 != "") {
        mysql_query("INSERT INTO `dentsply`.`t_telephone` (`teleTypeID`, `telephone`) VALUES ('$telType2', '$tel2')") or die ("Error: Can`t add telephone! " . mysql_error());
        $telID2 = mysql_insert_id();
        mysql_query("INSERT INTO `dentsply`.`t_telelist` (`teleID`, `clinicID`, `staffID`) VALUES ('$telID2', '$clinicID', '0')") or die ("Error: Can`t connect telephone to clinic! " . mysql_error());
    }
    if ($docTel3 != "") {
        mysql_query("INSERT INTO `dentsply`.`t_telephone` (`teleTypeID`, `telephone`) VALUES ('$telType3', '$tel3')") or die ("Error: Can`t add telephone! " . mysql_error());
        $telID3 = mysql_insert_id();
        mysql_query("INSERT INTO `dentsply`.`t_telelist` (`teleID`, `clinicID`, `staffID`) VALUES ('$telID3', '$clinicID', '0')") or die ("Error: Can`t connect telephone to clinic! " . mysql_error());
    }
    //Adding email to t_email
    mysql_query("INSERT INTO `dentsply`.`t_email` (`email`) VALUES ('$email')") or die ("Error: Can`t add email! " . mysql_error());
    $emailID = mysql_insert_id();
    //Connecting email to clinic via t_emaillist
    mysql_query("INSERT INTO `dentsply`.`t_emaillist` (`emailID`, `clinicID`, `staffID`) VALUES ('$emailID', '$clinicID', '0')") or die ("Error: Can`t connect email to clinic! " . mysql_error());
?>