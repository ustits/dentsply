<?php
    session_start();
        
    if (isset($_SESSION['login'])) {
        $login = $_SESSION['login'];
        $user = $_SESSION['name'];
        $userID = $_SESSION['id'];
        $loggedin = TRUE;
    }else $loggedin = FALSE;
        
    if ($loggedin) {
        $arr = array('log' => 'logged', 'name' => $user, 'id' => $userID);
        $json = json_encode($arr);
        echo $json;
    }else {
        echo 'notlogged';
    }

    //session_destroy();
?>