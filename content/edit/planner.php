<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $('.tasksTable table').hide();
    $('.search').hide();
    $("#tableType").select2({
        placeholder: "-Таблица-",
        width: "resolve",
        minimumResultsForSearch: "-1"
    });
    
   function buildEditTable() {
        var tableType = $('#tableType').val();
        if (tableType == 'doctors') {
            $.post('../../scripts/php/editHeaderScripts.php', {tableType: tableType}, function(response) {
                $('.list').before(response);
                $('.tasksTable table').show();
                $('.search').show();
                $.post('../../scripts/php/editScripts.php', {tableType: tableType}, function(response) {
                    $('.list').html(response);
                    $('.edit').editable('../../scripts/php/tables/edit.php', {
                        tooltip: 'Нажмите, чтобы изменить поле'
                    });
                    $('.edit2').editable('../../scripts/php/tables/edit.php', {
                        id: 'id2',
                        name: 'value2',
                        data: "{'A+': 'A+', 'A': 'A', 'B': 'B', 'C': 'C'}",
                        type: 'select',
                        submit: 'OK',
                        tooltip: 'Нажмите, чтобы изменить поле'
                    });
                    $('.edit3').editable('../../scripts/php/tables/edit.php', {
                        id: 'id3',
                        name: 'value2',
                        data: "{'0': 'Да', '1': 'Нет'}",
                        type: 'select',
                        submit: 'OK',
                        tooltip: 'Нажмите, чтобы изменить поле'
                    });
                });
            });
            
        }
    }
</script>
<fieldset>
    <legend>Редактирование</legend>
    <div class="tasksTable">
        <div id="test">
            <input type="text" class="search" placeholder="Поиск" />
            <select name="tableType" id="tableType">
                <option></option>
                <option value="doctors">Доктора</option>
                <option value="clinics">Клиники</option>
                <option value="users">Пользователи</option>
            </select>
            <input type="button" onClick="buildEditTable();" value="Построить"/>
            <br/><br/>
            <table>
                <tbody class="list">
                </tbody>
            </table>
            <p class="pagination"></p>
        </div>
    </div>
    <script>
        var options = {
            valueNames: ['name', 'id', 'progress']
        };
    
        var userList = new List('test', options);
</script>
</fieldset>

