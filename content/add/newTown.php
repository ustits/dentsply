<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $("#region").select2({
        placeholder: "-Выберите регион-",
        width: "resolve"
    });
    
    function createTown() {
        var vTownName = $(':input[name=townName]').val();
        var vRegion = $(':input[name=region]').val();
        var data = {
            townName: vTownName,
            region: vRegion
        };
        
        $.post('../../scripts/php/newTown.php', data);
        
        //Clearing form
        $(":input[name=townName]").val("");
        $("#region").select2("val", "");
    }
</script>
<fieldset>
    <legend>Новый город</legend>
    <form action="content/newTown.php" method="post" name="town" id="town">
        <div>
            <label for="townName">Город:</label>
            <input type="text" name="townName">
        </div>
        <div>
            <label for="region">Регион:</label>
            <select name="region" id="region">
                <option></option>
                <?php 
                    $query = "SELECT id, regionName FROM t_regions";
                    $result = mysql_query($query);
                    while ($row_value = mysql_fetch_row($result))
                    {
                        echo "<option value=$row_value[0]>$row_value[1]</option>";    
                    }
                ?> 
            </select>
        </div>
        <div>
            <label></label>
            <input type="submit" name="add" onClick="createTown();" value="Добавить">
        </div>
    </form>
</fieldset>