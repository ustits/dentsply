<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $("#telform2").hide();
    $("#telform3").hide();
    
    $("#clinicName").select2({
        placeholder: "-Выберите клинику-",
        width: "resolve",
        minimumInputLength: 2
    });
    $("#authority").select2({
        width: "resolve",
        minimumResultsForSearch: "-1"
    });
    $("#worksNow").select2({
        minimumResultsForSearch: "-1",
        width: "resolve"
    });
    $("#telType1").select2({
        minimumResultsForSearch: "-1",
        width: "resolve"
    });
    $("#telType2").select2({
        minimumResultsForSearch: "-1",
        width: "resolve"
    });
    $("#telType3").select2({
        minimumResultsForSearch: "-1",
        width: "resolve"
    });
    
    function createTelForm(tel) {
        if (tel == 1) {
            $("#telform2").show();
            $("#addTel1").hide();
        }else if (tel == 2) {
            $("#telform3").show();
            $("#addTel2").hide();
            $("#delTel1").hide();
        }
    }
        
    function deleteTelForm(tel) {
        if (tel == 1) {
            $("#telform2").hide();
            $("#addTel1").show();
            $("#docTel2").val("");
            $("#telType2").select2("val", "1");
        }else if (tel == 2) {
            $("#telform3").hide();
            $("#addTel2").show();
            $("#delTel1").show();
            $("#docTel3").val("");
            $("#telType3").select2("val", "1");
        }
    }
    
    $('#doctor').validate({
        submitHandler: function(){ createDoctor(); },
        rules: {
            docName: "required",
            clinicName: "required",
            docTel1: "required",
            workGraph: "required"
        },
        messages: {
            docName: "Введите Ф.И.О. доктора",
            clinicName: "Введите места работы",
            docTel1: "Введите номер телефона",
            workGraph: "Введите график работы"
        }
    });
    
    function createDoctor() {
        var vDocName = $(':input[name=docName]').val();
        var vClinicID = $('#clinicName').val();
        var vDocTel1 = $('#docTel1').val();
        var vTelType1 = $('#telType1').val();
        var vDocEmail = $(':input[name=docEmail]').val();
        var vCabinet = $(':input[name=cabinet]').val();
        var vWG = $(':input[name=workGraph]').val();
        var vWN = $('#worksNow').val();
        var vBirthday = $(':input[name=birthday]').val();
        var vNotes = $(':input[name=notes]').val();
        if ($('#docTel2').val() == "") {
            var vDocTel2 = "";
            var vTelType2 = "";
        } else {
            var vDocTel2 = $('#docTel2').val();
            var vTelType2 = $('#telType2').val();
        }
        if ($('#docTel3').val() == "") {
            var vDocTel3 = "";
            var vTelType3 = "";
        } else {
            var vDocTel3 = $('#docTel3').val();
            var vTelType3 = $('#telType3').val();
        }
        var data = {
            docName: vDocName,
            clinicID: vClinicID,
            docTel1: vDocTel1,
            telType1: vTelType1,
            docTel2: vDocTel2,
            telType2: vTelType2,
            docTel3: vDocTel3,
            telType3: vTelType3,
            docEmail: vDocEmail,
            cabinet: vCabinet,
            workGraph: vWG,
            worksNow: vWN,
            birthday: vBirthday,
            notes: vNotes
        };
      
    $.post('../../scripts/php/newDoctor.php', data);
        
    //Clearing form
    $(":input[name=docName]").val("");
    $("#clinicName").select2("val", "");
    $("#docTel1").val("");
    $("#telType1").select2("val", "1");
    $("#docTel2").val("");
    $("#telType2").select2("val", "1");
    $("#docTel3").val("");
    $("#telType3").select2("val", "1");
    $(":input[name=docEmail]").val("");
    $(":input[name=cabinet]").val("");
    $(":input[name=workGraph]").val("");
    $("#worksNow").select2("val", "yes");
    $(":input[name=birthday]").val("");
    $(":input[name=notes]").val("");
         
    deleteTelForm(2);
    deleteTelForm(1);
    }
    
</script>

<fieldset id="doctorField">
    <legend>Новый доктор</legend>
    <form action="index.php" method="post" name="doctor" id="doctor">
        <div>
            <label for="docName">Ф.И.О.: *</label>
            <input type="text" name="docName" required>
        </div>
        <div>
            <label for="clinicName">Место работы: *</label>
            <select name="clinicName" id="clinicName" multiple>
                <option></option>
                <?php
                    $query = "SELECT id, clinicName FROM t_clinics";
                    $result = mysql_query($query);
                    
                    while($row_value = mysql_fetch_row($result))
                    {
                        echo "<option value=$row_value[0]>$row_value[1]</option>";
                    }
                ?>
            </select>
        </div>
        <div id="telform1">
            <label for="docTel1">Телефон1: *</label>
            <input type="text" name="docTel1" id="docTel1">
            <select name="telType1" id="telType1" style="width: 100px">
                <option value="1">Моб. телефон</option>
                <option value="2">Раб. телефон</option>
            </select>
            <input type="button" name="addTel1" id="addTel1" onClick="createTelForm(1);" class="tel" value="+">
        </div>
        <div id="telform2">
            <label for="docTel2">Телефон2:</label>
            <input type="text" name="docTel2" id="docTel2">
            <select name="telType2" id="telType2" style="width: 100px">
                <option value="1">Моб. телефон</option>
                <option value="2">Раб. телефон</option>
            </select>
            <input type="button" name="addTel2" id="addTel2" onClick="createTelForm(2);" class="tel" value="+">
            <input type="button" name="delTel1" id="delTel1" onClick="deleteTelForm(1);" class="tel" value="x">
        </div>
        <div id="telform3">
            <label for="docTel3">Телефон3:</label>
            <input type="text" name="docTel3" id="docTel3">
            <select name="telType3" id="telType3" style="width: 100px">
                <option value="1">Моб. телефон</option>
                <option value="2">Раб. телефон</option>
            </select>
            <input type="button" name="delTel2" id="delTel2" onClick="deleteTelForm(2);" class="tel" value="x">
        </div>
        <div>
            <label for="docEmail">Email:</label>
            <input type="email" name="docEmail">
        </div>
        <div>
            <label for="cabinet">Кабинет:</label>
            <input type="text" name="cabinet">
        </div>
        <div>
            <label for="workGraph">График работы: *</label>
            <textarea name="workGraph"></textarea>
        </div>
        <div>
            <label for="authority">Авторитет:</label>
            <select name="authority" id="authority">
                <option>A+</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>
        <div>
            <label for="worksNow">Работает:</label>
            <select name="worksNow" id="worksNow">
                <option value="1">Да</option>
                <option value="0">Нет</option>
            </select>
        </div>
        <div>
            <label for="birthday">День рождения:</label>
            <input type="date" name="birthday">
        </div>
        <div>
            <label for="notes">Заметки:</label>
            <textarea name="notes"></textarea>
        </div>
        <div>
            <label></label>
            <input type="submit" name="add" value="Добавить">
        </div>
    </form>
</fieldset>