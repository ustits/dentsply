<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $("#telform2").hide();
    $("#telform3").hide();
    
    /*                  Select2                     */
    $("#network").select2({
        placeholder: "-Выберите сеть-",
        width: "resolve",
        minimumInputLength: 2
    });
    $("#town").select2({
        placeholder: "-Выберите город-",
        width: "resolve"
    });
    $("#category").select2({
        placeholder: "-Выберите категорию-",
        minimumResultsForSearch: "-1",
        width: "resolve"
    });
    $("#priceLevel").select2({
        placeholder: "-Выберите уровень-",
        minimumResultsForSearch: "-1",
        width: "resolve"
    });
    $("#orderDays").select2({
        placeholder: "-Выберите дни-",
        minimumResultsForSearch: "-1",
        width: "resolve"
    });
    $("#worksNow").select2({
        minimumResultsForSearch: "-1",
        width: "resolve"
    });
    $("#telType1").select2({
        minimumResultsForSearch: "-1"
    });
    $("#telType2").select2({
        minimumResultsForSearch: "-1"
    });
    $("#telType3").select2({
        minimumResultsForSearch: "-1"
    });

    function createTelForm(tel) {
        if (tel == 1) {
            $("#telform2").show();
            $("#addTel1").hide();
        }else if (tel == 2) {
            $("#telform3").show();
            $("#addTel2").hide();
            $("#delTel1").hide();
        }
    }
    
    function deleteTelForm(tel) {
        if (tel == 1) {
            $("#telform2").hide();
            $("#addTel1").show();
            $("#tel2").val("");
            $("#telType2").select2("val", "1");
        }else if (tel == 2) {
            $("#telform3").hide();
            $("#addTel2").show();
            $("#delTel1").show();
            $("#tel3").val("");
            $("#telType3").select2("val", "1");
        }
    }
    
    /*                  Form verification                     */
    $('#clinics').validate({
        submitHandler: function(){ createClinic(); },
        rules: {
            clinicName: "required",
            town: "required",
            address: "required",
            category: "required",
            chairs: "required",
            doctorCount: "required",
            passability: "required",
            restoration: "required",
            canalsCount: "required",
            purchase: "required",
            priceLevel: "required",
            orderFreq: "required",
            orderDays: "required",
            tel1: "required",
            district: "required",
            email: "required",
            www: "required"
        },
        messages: {
            clinicName: "Введите название клиники",
            town: "Выберите город",
            address: "Введите адрес",
            category: "Выберите категорию",
            chairs: "Введите кол-во кресел",
            doctorCount: "Введите кол-во докторов",
            passability: "Введите проходимость",
            restoration: "Введите ср. кол-во реставраций",
            canalsCount: "Введите ср. кол-во каналов",
            purchase: "Введите закупки",
            priceLevel: "Выберите уровень цен",
            orderFreq: "Введите частоту заказов",
            orderDays: "Выберите дни заказов",
            tel1: "Введите контактный телефон",
            district: "Введите район",
            email: "Введите email",
            www: "Введите сайт"
        }
    });
    
    /*                  Forming info for a query                    */
    function createClinic() {
        var formData = $('#clinics').serialize();
        $.post('../../scripts/php/newClinic.php', formData, processData);
        
        $("#clinicName").val("");
        $("#network").select2("val", "");
        $("#town").select2("val", "");
        $(":input[name=address]").val("");
        $("#category").select2("val", "");
        $(":input[name=chairs]").val("");
        $(":input[name=doctorCount]").val("");
        $(":input[name=passability]").val("");
        $(":input[name=restoration]").val("");
        $(":input[name=canalsCount]").val("");
        $(":input[name=purchase]").val("");
        $("#priceLevel").select2("val", "");
        $(":input[name=orderFreq]").val("");
        $("#orderDays").select2("val", "");
        $(":input[name=tel1]").val("");
        $("#telType").select2("val", "");
        $(":input[name=district]").val("");
        $(":input[name=email]").val("");
        $(":input[name=www]").val("");
        $("#worksNow").select2("val", "1");
        $(":input[name=notes]").val("");
        
        deleteTelForm(2);
        deleteTelForm(1);
    };
</script>

<fieldset id="clinicField">
    <legend>Новая клиника</legend>
        <form action="content/newClinic.php" method="post" name="clinics" id="clinics">
            <div>
				<label for="clinicName">Клиника: *</label>
				<input type="text" name="clinicName" id="clinicName">
            </div>
            <div>
                <label for="network">Сеть:</label>
                <select name="network" id="network">
                    <option></option>
                <?php 
                    $query = "SELECT id, netName FROM t_net";
                    $result = mysql_query($query);
                    while ($row_value = mysql_fetch_row($result))
                    {
                        echo "<option value=$row_value[0]>$row_value[1]</option>";    
                    }
                ?>   
				</select>
            </div>
            <div>
				<label for="town">Город: *</label>
                <select name="town" id="town">
                    <option></option>>
                    <?php 
                        $query = "SELECT id, townName FROM t_towns";
                        $result = mysql_query($query);
    
                        while ($row_value = mysql_fetch_row($result))
                        {
                            echo "<option value=$row_value[0]>$row_value[1]</option>";    
                        }
                    ?>
                </select>
            </div>
            <div>
				<label for="address">Адрес: *</label>
				<input type="text" name="address">
            </div>
            <div>
				<label for="category">Категория: *</label>
				<select name="category" id="category">
				    <option></option>
				    <option>+A</option>
				    <option>A</option>
				    <option>B</option>
				    <option>C</option>
				</select>
            </div>
            <div>
				<label for="chairs">Кресла: *</label>
				<input type="number" name="chairs" min="0">
            </div>
            <div>
                <label for="doctorCount">Докторов: *</label>
                <input type="number" name="doctorCount" min="0">
            </div>
            <div>
                <label for="passability">Проходимость: *</label>
                <input type="number" name="passability" min="0">
            </div>
            <div>
                <label for="restoration">Ср. кол-во реставраций: *</label>
                <input type="number" name="restoration" min="0">
            </div>
            <div>
                <label for="canalsCount">Ср. кол-во каналов на кресло: *</label>
                <input type="number" name="canalsCount" min="0">
            </div>
            <div>
                <label for="purchase">Закупка(месяц): *</label>
                <input type="number" name="purchase" min="0">
            </div>
            <div>
				<label for="priceLevel">Уровень цен: *</label>
				<select name="priceLevel" id="priceLevel">
				    <option></option>
				    <option>Высокий</option>
				    <option>Средний</option>
				    <option>Низкий</option>
				</select>
            </div>
            <div>
                <label for="orderFreq">Частота заказов: *</label>
                <input type="number" name="orderFreq" min="0">
            </div>
            <div>
                <label for="orderDays">Дни заказов: *</label>
                <select name="orderDays" id="orderDays" multiple>
                    <option>Пн.</option>
                    <option>Вт.</option>
                    <option>Ср.</option>
                    <option>Чт.</option>
                    <option>Пт.</option>
                    <option>Сб.</option>
                    <option>Вс.</option>
                </select>
            </div>
            <div id="telform1">
				<label for="tel1">Телефон1: *</label>
				<input type="text" name="tel1" id="tel1">
                <select name="telType1" id="telType1" style="width: 100px">
                    <option value="2">Раб. телефон</option>
                    <option value="1">Моб. телефон</option>
                </select>
                <input type="button" name="addTel1" id="addTel1" onClick="createTelForm(1);" class="tel" value="+">
            </div>
            <div id="telform2">
				<label for="tel2">Телефон2:</label>
				<input type="text" name="tel2" id="tel2">
                <select name="telType2" id="telType2" style="width: 100px">
                    <option value="2">Раб. телефон</option>
                    <option value="1">Моб. телефон</option>
                </select>
                <input type="button" name="addTel2" id="addTel2" onClick="createTelForm(2);" class="tel" value="+">
                <input type="button" name="delTel1" id="delTel1" onClick="deleteTelForm(1);" class="tel" value="x">
            </div>
            <div id="telform3">
				<label for="tel3">Телефон3:</label>
				<input type="text" name="tel3" id="tel3">
                <select name="telType3" id="telType3" style="width: 100px">
                    <option value="2">Раб. телефон</option>
                    <option value="1">Моб. телефон</option>
                </select>
                <input type="button" name="delTel2" id="delTel2" onClick="deleteTelForm(2);" class="tel" value="x">
            </div>
            <div>
				<label for="district">Район/Метро: *</label>
				<input type="text" name="district">
            </div>
            <div>
				<label for="email">Email: *</label>
				<input type="email" name="email">
            </div>
            <div>
				<label for="www">WWW: *</label>
				<input type="text" name="www" value="http://">
            </div>
            <div>
                <label for="worksNow">Работает:</label>
                <select name="worksNow" id="worksNow">
                    <option value="1">Да</option>
                    <option value="0">Нет</option>
                </select>
            </div>
            <div>
				<label for="notes">Примечания:</label>
				<textarea name="notes"></textarea>
            </div>
            <div>
                <label></label>
                <input type="submit" name="postButton" value="Добавить">
            </div>

    </form>
</fieldset>