<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $("#contType").select2({
        placeholder: "-Тип контакта-",
        width: "resolve",
        minimumResultsForSearch: "-1"
    });
    
   /* $("select[name=contPurpose]").change(function() {
        if($("select[name=contPurpose]").val() == "Заказ"){
            $("#prodNameDiv").show();
            $("#quantityDiv").show();
            $("#distributorDiv").show();
            $("#notesDiv").show();
        }else if($("select[name=contPurpose]").val() != "Заказ") {
            $("#prodNameDiv").hide();
            $("#quantityDiv").hide();
            $("#distributorDiv").hide();
            $("#notesDiv").hide();
        }
    }).change();*/

    function createPlanner() {
        var vContactDate = $(':input[name=contactDate]').val();
        var vContact = $(':input[name=contact]').val();
        var vContType = $('#contType').val();
        var vContPurpose = $(":input[name=contPurpose]").val();
        var vNextContact = $(':input[name=nextContact]').val();
        var vNextContactPurpose = $(':input[name=nextContactPurpose]').val();
        var vNotes = $(':input[name=notes]').val();
        
        var data = {
            contactDate: vContactDate,
            contact: vContact,
            contType: vContType,
            contPurpose: vContPurpose,
            nextContact: vNextContact,
            nextContactPurpose: vNextContactPurpose,
            notes: vNotes,
            userID: userID
        };

        $.post('../../scripts/php/newPlanner.php', data, function(response) {
            alert(response);
        });
        
        $(":input[name=contactDate]").val("");
        $(":input[name=contact]").val("");
        $("#contType").val("");
        $(":input[name=contPurpose]").val("");
        $(":input[name=nextContact]").val("");
        $(":input[name=nextContactPurpose]").val("");
        $(":input[name=notes]").val("");
    }
    
</script>
<fieldset name=plannerField>
    <legend>Планировщик</legend>
    <form action="index.php" method="post" name="planner" id="planner">
        <div>
            <label for="contactDate">Дата:</label>
            <input type="date" name="contactDate">
        </div>
        <div>
            <label for="contact">Контактное лицо</label>
            <input type="text" name="contact">
        </div>
        <div>
            <label for="contType">Тип контакта:</label>
            <select name="contType" id="contType">
                <option></option>
                <option>Визит</option>
                <option>Звонок</option>
            </select>
        </div>
        <div>
            <label for="contPurpose">Цель контакта:</label>
            <textarea name="contPurpose"></textarea>
        </div>
        <div>
            <label for="nextContact">След. контакт:</label>
            <input type="date" name="nextVisit">
        </div>
        <div>
            <label for="nextContactPurpose">Цель след. контакта:</label>
            <textarea name="nextContactPurpose"></textarea>
        </div>
        <div id="notesDiv">
            <label for="notes">Примечание:</label>
            <textarea name="notes"></textarea>
        </div>
        <div>
            <label></label>
            <input type="button" name="add" onClick="createPlanner();" value="Добавить">
        </div>
    </form>
</fieldset>