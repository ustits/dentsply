<?php 
    header("Content-Type: text/html; charset=utf-8"); 
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply'); 

/*
It has some errors in scripts, because of clinics being Array
*/
?>
<script>
    $("#distributor").select2({
        placeholder: "-Выберите-",
        width: "resolve",
        minimumInputLength: 2
    });
    $("#subdistributor").select2({
        placeholder: "-Выберите-",
        width: "resolve",
        minimumInputLength: 2
    });
    $("#prodName").select2({
        placeholder: "-Выберите продукт-",
        width: "resolve",
        minimumInputLength: 2
    });
    
    function createOrder() {
        var vDate = $(':input[name=date]').val();
        var vContact = $(':input[name=contact]').val();
        var vProdName = $('#prodName').val();
        var vQuant = $(':input[name=quantity]').val();
        var vDistr = $('#distributor').val();
        var vSubDistr = $('#subdistributor').val();
        var vNotes = $(':input[name=notes]').val();
        var data = {
            date: vDate,
            contact: vContact,
            prodName: vProdName,
            quantity: vQuant,
            distributor: vDistr,
            subdistributor: vSubDistr,
            notes: vNotes,
            userID: userID
        };
        
        $.post('../../scripts/php/newOrder.php', data, function(response) {
            alert(response);
        });
        
        //Clearing form
        $(":input[name=date]").val("");
        $(":input[name=contact]").val("");
        $("#prodName").select2("val", "");
        $(":input[name=quantity]").val("");
        $("#distributor").select2("val", "");
        $("#subdistributor").select2("val", "");
        $(":input[name=notes]").val("");
    }
</script>
<fieldset id="orderField">
    <legend>Заказы</legend>
        <form action="index.php" method="post" name="orders" id="orders">
            <div>
                <label for="date">Дата:</label>
                <input type="date" name="date">
            </div>
            <div>
                <label for="contact">Контактное лицо:</label>
                <input type="text" name="contact">
            </div>
            <div>
                <label for="prodName">Продукт:</label>
                <select name="prodName" id="prodName" multiple>
                    <option></option>
                    <?php 
                        $query = "SELECT id, productName FROM t_product";
                        $result = mysql_query($query);
    
                        while ($row_value = mysql_fetch_row($result))
                        {
                            echo "<option value=$row_value[0]>$row_value[1]</option>";    
                        }
                    ?>
                </select>
            </div>
            <div>
				<label for="quantity">Количество:</label>
				<input type="number" name="quantity" min="0">
            </div>
            <div>
                <label for="distributor">Дистрибьютор:</label>
                <select name="distributor" id="distributor">
                    <option></option>
                    <?php 
                        $query = "SELECT id, ruName FROM t_distributor WHERE isSub = 0";
                        $result = mysql_query($query);
    
                        while ($row_value = mysql_fetch_row($result))
                        {
                            echo "<option value=$row_value[0]>$row_value[1]</option>";    
                        }
                    ?>
                </select>
            </div>
            <div>
                <label for="subdistributor">Сабдистрибьютор:</label>
                <select name="subdistributor" id="subdistributor">
                    <option></option>
                    <?php 
                        $query = "SELECT id, ruName FROM t_distributor WHERE isSub = 1";
                        $result = mysql_query($query);
    
                        while ($row_value = mysql_fetch_row($result))
                        {
                            echo "<option value=$row_value[0]>$row_value[1]</option>";    
                        }
                    ?>
                </select>
            </div>
            <div>
				<label for="notes">Примечание:</label>
				<textarea name="notes"></textarea>
            </div>
            <div>
                <label></label>
                <input type="button" name="postButton" onClick="createOrder();" value="Добавить">
            </div>
        </form>
</fieldset>﻿