<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $("#location").select2({
        placeholder: "-Место проведения-",
        width: "resolve",
        minimumInputLength: 2
    });
    
    function createCourse() {
        var vCourseName = $(':input[name=courseName]').val();
        var vCourseDate = $(':input[name=courseDate]').val();
        var vBegin = $(':input[name=begin]').val();
        var vEnd = $(':input[name=end]').val();
        var vLocation = $(':input[name=location]').val();
        var data = {
            courseName: vCourseName,
            courseDate: vCourseDate,
            begin: vBegin,
            end: vEnd,
            location: vLocation
        };
        
        $.post('../../scripts/php/newCourse.php', data);
    }
</script>
<fieldset name="courseField">
    <legend>Новые курсы</legend>
    <form action="content/newCourse.php" method="post" name="course" id="course">
        <div>
            <label for="courseName">Название курса:</label>
            <input type="text" name="courseName">
        </div>
        <div>
            <label for="courseDate">Дата:</label>
            <input type="date" name="courseDate">
        </div>
        <div>
            <label for="begin">Начало:</label>
            <input type="text" name="begin">
        </div>
        <div>
            <label for="end">Конец:</label>
            <input type="text" name="end">
        </div>
        <div>
            <label for="location">Место проведения:</label>
            <select name="location" id="location">
                <option></option>
                <?php
                    $query = "SELECT id, clinicName FROM t_clinics";
                    $result = mysql_query($query);
                    
                    while($row_value = mysql_fetch_row($result))
                    {
                        echo "<option value=$row_value[0]>$row_value[1]</option>";
                    }
                ?>
            </select>
        </div>
        <div>
            <label></label>
            <input type="submit" name="add" onClick="createCourse();" value="Добавить">
        </div>
    </form>
</fieldset>