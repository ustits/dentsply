<?php 
    header("Content-Type: text/html; charset=utf-8"); 
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply'); 
?>
<script>
    $("#netClinics").select2({
        placeholder: "-Выберите больницы-",
        width: "resolve",
        minimuInputLength: 2
    });
    
    function createNetwork() {
        var vNetName = $(":input[name=netName]").val();
        var vNetClinics = $("#netClinics").val();
        var data = {
            netName: vNetName,
            netClinics: vNetClinics
        };
        
        $.post('../../scripts/php/newNetwork.php', data);
        
        //Clearing form
        $(":input[name=netName]").val("");
        $("#netClinics").select2("val", "");
    }
</script>
<fieldset id="networkField">
    <legend>Новая сеть</legend>
    <form action="index.php" method="post" name="network" id="network">
        <div>
            <label for="netName">Сеть:</label>
            <input type="text" name="netName">
        </div>
        <div>
            <label for="netClinics">Клиники:</label>
            <select name="netClinics" id="netClinics" multiple>
                <option></option>
				<?php
                    $query = "SELECT id, clinicName FROM t_clinics";
                    $result = mysql_query($query);

                    while ($row_value = mysql_fetch_row($result)) {
                        echo "<option value=$row_value[0]>$row_value[1]</option>";
                    }
                ?>
            </select>
        </div>
        <div>
            <label></label>
            <input type="submit" name="add" onClick="createNetwork();" value="Добавить">
        </div>
    </form>
</fieldset>