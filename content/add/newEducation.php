<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $("#staffName").select2({
        placeholder: "-Выберите доктора-",
        width: "resolve",
        minimumInputLength: 2
    });
    $("#courseName").select2({
        placeholder: "-Выберите курс-",
        width: "resolve"
    });
    
    function createEducation() {
        var vStaffName = $(':input[name=staffName]').val();
        var vCourseName = $(':input[name=courseName]').val();
        var data = {
            staffName: vStaffName,
            courseName: vCourseName
        };
        
        $.post('../../scripts/php/newEducation.php', data);
    }
</script>
<fieldset name="educationField">
    <legend>Запись на курсы</legend>
    <form method="post" action="content/newEducation.php">
        <div>
            <label for="staffName">Доктор:</label>
            <select name="staffName" id="staffName">
                <option></option>
                <?php
                    $query = "SELECT name FROM t_staff WHERE staffTypeID = '1'";
                    $result = mysql_query($query);
                        
                    while($row_value = mysql_fetch_row($result))
                    {
                        echo "<option>$row_value[0]</option>";
                    }
                ?>
            </select>
        </div>
        <div>
            <label for="courseName">Название курса:</label>
            <select name="courseName" id="courseName">
                <option></option>
                <?php
                    $query = "SELECT courseName FROM t_course";
                    $result = mysql_query($query);
                        
                    while($row_value = mysql_fetch_row($result))
                    {
                        echo "<option>$row_value[0]</option>";
                    }
                ?>
            </select>
        </div>
        <div>
            <label></label>
            <input type="submit" name="add" onClick="createEducation();" value="Добавить">
        </div>
    </form>
</fieldset>