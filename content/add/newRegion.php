<?php 
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');
?>
<script>
    function createRegion() {
        var vRegionName = $(':input[name=regionName]').val();
        $.post('../../scripts/php/newRegion.php', vRegionName);
    }
</script>
<fieldset>
    <legend>Новый регион</legend>
    <form>
        <div>
            <label for="regionName">Регион:</label>
            <input type="text" name="regionName">
        </div>
        <div>
            <label></label>
            <input type="submit" name="add" onClick="createRegion();" value="Добавить">
        </div>
    </form>
</fieldset>