<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>

<script>
    $("#to").select2({
        placeholder: "-Выберите сотрудника-",
        width: "resolve"
    });
    
    $('#newTask').validate({
        submitHandler: function(){ createTask(); },
        rules: {
            to: "required",
            task: "required"
        },
        messages: {
            to: "Выберите кому назначить задачу",
            task: "Опишите задачу"
        }
    });
  
    /*                  Forming info for a query                    */
    function createTask() {
        var formData = $('#newTask').serialize() + '&from=' + userID;
        $.post('../../scripts/php/newTask.php', formData, processData);
        
        $("#to").select2("val", "");
        $(":input[name=task]").val("");
    }
    
</script>

<fieldset>
    <legend>Новая задача</legend>
    <form action="index.php" method="post" name="newTask" id="newTask">
        <div>
            <label for="to">Кому:</label>
            <select name="to" id="to" multiple>
                <option></option>
                <?php 
                    $query = "SELECT id, name, secondName FROM t_users";
                    $result = mysql_query($query);
                    while ($row_value = mysql_fetch_row($result))
                    {
                        echo "<option value=$row_value[0]>$row_value[1] $row_value[2]</option>";    
                    }
                ?>   
            </select>
        </div>
        <div>
            <label for="task">Задача:</label>
            <textarea name="task"></textarea>
        </div>
        <div>
            <label></label>
            <input type="submit" name="postButton" value="Добавить">
        </div>
    </form>
</fieldset>