<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $('.tasksTable').hide();
    
    $.post('../../scripts/php/getTasks.php', {id: userID}, function(response) {
        if (response == '0') {
            $('fieldset').append('<p>Нет задач</p>');
        } else if (response > 0) {
            $('.tasksTable').show();
            $.post('../../scripts/php/fillTable.php', {id: userID}, function(response) {
                $('.tasksTable table').append(response);
            });
        } else {
            $('fieldset').append('<p>Ошибка: Невозможно подключиться к серверу!</p>');
        }
    });
</script>
<fieldset>
    <legend>Список задач</legend>
    <div class="tasksTable">
        <table>
            <tr>
                <th>№</th>
                <th>От кого</th>
                <th>Задача</th>
                <th>Статус</th>
            </tr>
        </table>
    </div>        
</fieldset>