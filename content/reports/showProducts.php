<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  

?>

<fieldset>
    <legend>Список продуктов</legend>
    <div class="tasksTable">
        <div id="test">
            <input type="text" class="search" placeholder="Поиск" /> 
            <br/><br/>
            <table>
                <tr>
                    <th class="sort" data-sort="id">№</th>
                    <th class="sort" data-sort="name">Название</th>
                    <th class="sort" data-sort="section">Раздел</th>
                    <th class="sort" data-sort="subsection">Подраздел</th>
                </tr>
                <tbody class="list">        
                    <?php
                        $query = "SELECT id, productName, section, subsection FROM t_product";
                        $result = mysql_query($query);
                        while ($row_value = mysql_fetch_row($result)) {
                            echo "<tr>";
                            echo "<td class='id'>$row_value[0]</td>";
                            echo "<td class='name'>$row_value[1]</td>";
                            echo "<td class='section'>$row_value[2]</td>";
                            echo "<td class='subsection'>$row_value[3]</td>";
                        }
                    ?>
                </tbody>
            </table>
            <ul class="pagination"></ul>
        </div>
    </div>   
    <script>
    var options = {
        valueNames: ['name', 'id', 'section', 'subsection'],
        page: 10,
        plugins: [ListPagination({})]
    };
    
    var userList = new List('test', options);
</script>
</fieldset>