<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  
?>
<script>
    $('.tasksTable table').hide();
    $('.search').hide();
    $("#interval").select2({
        placeholder: "-Промежуток-",
        width: "resolve",
        minimumResultsForSearch: "-1"
    });
    
    function buildProgress() {
        var interval = $('#interval').val();
        $.post('../../scripts/php/buildProgress.php', {inteval: interval}, function(response) {
            $('.list').html(response);
            $('.tasksTable table').show();
            $('.search').show();
        });
    }
</script>
<fieldset>
    <legend>Прогресс сотрудников</legend>
    <div class="tasksTable">
        <div id="test">
            <input type="text" class="search" placeholder="Поиск" />
            <select name="interval" id="interval">
                <option></option>
                <option>За день</option>
                <option>За неделю</option>
                <option>За месяц</option>
            </select>
            <input type="button" onClick="buildProgress();" value="Построить"/>
            <br/><br/>
            <table>
                <tr>
                    <th class="sort" data-sort="id">№</th>
                    <th class="sort" data-sort="name">Сотрудник</th>
                    <th class="sort" data-sort="progress">Прогресс</th>
                </tr>
                <tbody class="list">        
                    <?php
                        $query = "SELECT id, name, secondName FROM t_users";
                        $result = mysql_query($query);

                        while ($row_value = mysql_fetch_row($result)) {
                            //Getting number of orders/visits user have made
                            $query = "SELECT COUNT(*) FROM t_progress WHERE userID = $row_value[0]";
                            $progress = mysql_fetch_row(mysql_query($query));
                            echo "<tr>";
                            echo "<td class='id'>$row_value[0]</td>";
                            echo "<td class='name'>$row_value[1] $row_value[2]</td>";
                            echo "<td class='progress'>$progress[0]</td>";
                        }
                    ?>
                </tbody>
            </table>
            <p class="pagination"></p>
        </div>
    </div>   
    <script>
    var options = {
        valueNames: ['name', 'id', 'progress']
    };
    
    var userList = new List('test', options);
</script>
</fieldset>