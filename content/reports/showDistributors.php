<?php
    header("Content-Type: text/html; charset=utf-8");
    require '../../scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');  

?>

<fieldset>
    <legend>Список дистрибьюторов</legend>
    <div class="tasksTable">
        <div id="test">
            <input type="text" class="search" placeholder="Поиск" /> 
            <br/><br/>
            <table>
                <tr>
                    <th class="sort" data-sort="id">№</th>
                    <th class="sort" data-sort="name">Название</th>
                </tr>
                <tbody class="list">        
                    <?php
                        $query = "SELECT id, ruName FROM t_distributor";
                        $result = mysql_query($query);
                        while ($row_value = mysql_fetch_row($result)) {
                            echo "<tr>";
                            echo "<td class='id'>$row_value[0]</td>";
                            echo "<td class='name'>$row_value[1]</td>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>   
    <script>
    var options = {
        valueNames: ['name', 'id']
    };
    
    var userList = new List('test', options);
</script>
</fieldset>