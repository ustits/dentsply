<?php 
    header("Content-Type: text/html; charset=utf-8");
    require 'scripts/php/scripts.php';
    Connection('localhost', 'root', '', 'dentsply');
?>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style/style.css" />
        <link href="scripts/select2-3.4.3/select2.css" rel="stylesheet"/>
        <script src="scripts/jquery-1.10.js"></script>
        <script type="text/javascript" src="scripts/script.js"></script>
        <script src="scripts/select2-3.4.3/select2.js"></script>
        <script src="scripts/jquery.validate.min.js"></script>
        <script>      
            function errorResponse() {
                $('#loginError').text('Невозможно подключиться к серверу!'); 
            }
            
            function processData(response) {
                alert(response);
                if (response == 'pass') {
                    window.location.replace("index.php");
                }else if (response == 'fail') {
                    $('#loginError').text('Неверный логин или пароль!');
                }else {
                   $('#loginError').text('Невозможно подключиться к серверу!'); 
                }
            }
            
            function getAccess() {
                var vLogin = $(':input[name=loginField]').val();
                var vPassword = $(':input[name=passwordField]').val();
                var data = {
                    login: vLogin,
                    password: vPassword
                };
                
                $.post('scripts/php/login.php', data, processData) . error(errorResponse);

            }
        </script>
        <title>Логин</title>
    </head>
    <body>
        <div class="login-wrapper">
            <form method="post" action="login.php" name="login" class="login" id="login">
                <h1>Dentsply</h1>
                <div>
                    <label for="loginField">Логин:</label> 
                    <input type="text" name="loginField" id="loginField">
                </div>
                <div>
                    <label for="passwordField">Пароль:</label>
                    <input type="password" name="passwordField" id="passwordField">
                </div>
                <div>
                    <p id="loginError"></p>
                    <input type="submit" value="Войти" id="login-button">
                </div>
            </form>
            <script>
                $('#login').validate({
                    submitHandler: function(){ getAccess(); },
                    rules: {
                        loginField: "required",
                        passwordField: "required"
                    },
                    messages: {
                        loginField: "Введите имя пользователя",
                        passwordField: "Введите пароль"
                    }
                });
            </script>
        </div>
    </body>
</html>
